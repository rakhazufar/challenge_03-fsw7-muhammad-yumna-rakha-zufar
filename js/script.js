$(".owl-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    992: {
      items: 3,
    },
  },
});

const prev = document.querySelector(".owl-prev");
prev.innerHTML = "<img src='./assets/Left-button.svg' alt=''>";

const next = document.querySelector(".owl-next");
next.innerHTML = "<img src='./assets/Right-button.svg' alt=''>";
